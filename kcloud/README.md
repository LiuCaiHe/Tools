#kcloud
防骗云平台工具，区分站长版和开发者版，请注意使用对应版本，开发者版需要云端授权，非已授权开发者请勿下载使用，否则后果自负。

|    文件     |        说明         |
| ----------- | ------------------- |
|站长版|站长版防骗云平台，适用于多数站长|
|开发者版|仅供开发者使用，须授权后才可正常使用|